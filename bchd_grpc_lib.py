#!/usr/bin/env python3


# Requires:
# python -m pip install grpcio
# python -m pip install grpcio-tools

# Clone BCHD https://github.com/gcash/bchd/
# Run the following inside ~/bchd/bchrpc/pb-py
# python3 -m grpc_tools.protoc -I=./ --python_out=./pb-py --grpc_python_out=./pb-py ./bchrpc.proto

# Libs imported here are located at ~/bchd/bchrpc/pb-py


import grpc
import bchrpc_pb2 as pb
import bchrpc_pb2_grpc as bchrpc


grpc_server = "bchd.greyh.at:8335"
#grpc_server = "bchd.imaginary.cash:8335"
#grpc_server = "bchd-testnet.greyh.at:18335"
#grpc_server = "bchd.ny1.simpleledger.io:8335"
#grpc_server = "bchd3.prompt.cash:8335"


class HitGrpc:
    def call_channel(self, func):
        """Decorator to call the functions easier while keeping the with statement
        """
            def wrapper(*args, **kwargs):
                with grpc.secure_channel(grpc_server, grpc.ssl_channel_credentials()) as channel:
                    stub = bchrpc.bchrpcStub(channel)
                    return func(stub, *args, **kwargs)
            return wrapper

    def get_mempool_info(self, stub):
        """ Returns the state of the current mempool.
        """
        req = pb.GetMempoolInfoRequest()
        return stub.GetMempoolInfo(req)


    def get_mempool(self, stub):
        """ Returns information about all transactions currently in the memory pool.
        """
        req = pb.GetMempoolRequest()
        return stub.GetMempool(req)


    def get_blockchain_info(self, stub):
        """ Returns information about all transactions currently in the memory pool.
        """
        req = pb.GetBlockchainInfoRequest()
        return stub.GetBlockchainInfo(req)



    def get_block_info_by_height(self, stub, height):
        """ Returns metadata and info for a specified block when height is sent
        """
        req = pb.GetBlockInfoRequest()
        req.height = height
        return stub.GetBlockInfo(req)
            

    def get_block_info_by_hash(self, stub, block_hash):
        """ Returns metadata and info for a specified block when hex hash is sent
        """
        req = pb.GetBlockInfoRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(block_hash)[::-1]
        return stub.GetBlockInfo(req)

    def get_block_by_height(self, stub, height):
        """ Returns detailed data for a block
        """
        req = pb.GetBlockRequest()
        req.height = height
        return stub.GetBlock(req)

    def get_block_by_hash(self, stub, block_hash):
        """ Returns detailed data for a block
        """
        req = pb.GetBlockRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(block_hash)[::-1]
        return stub.GetBlock(req)
 
    def get_raw_block_by_height(self, stub, height):
        """ Returns a block in a serialized format provided block height 
        """
        req = pb.GetBlockInfoRequest()
        req.height = height
        return stub.GetRawBlock(req)
 
    def get_raw_block_by_hash(self, stub, block_hash):
        """ Returns a block in a serialized format provided block hash 
        """
        req = pb.GetBlockInfoRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(block_hash)[::-1]
        return stub.GetRawBlock(req)


    def get_block_filter_by_height(self, stub, height):
        """ Returns the compact filter (cf) of a block as a Golomb-Rice encoded set.
        when given a block height
        **Requires CfIndex**
        """
        req = pb.GetBlockFilterRequest()
        req.height = height
        return stub.GetBlockFilter(req)

    def get_block_filter_by_hash(self, stub, block_hash):
        """ Returns the compact filter (cf) of a block as a Golomb-Rice encoded set.
        when given a block hash
        **Requires CfIndex**
        """
        req = pb.GetBlockFilterRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(block_hash)[::-1]
        return stub.GetBlockFilter(req)

    # FIXME - not implemeted
    def get_headers(self):
        """Takes a block locator object and returns a batch of no more than 2000 headers.
        accepts 'block_locator_hashes', 'stop_hash'
        """
        req = pb.GetHeadersRequest()
        req.block_locator_hashes = None
        req.stop_hash = None
        #return stub.GetHeaders(req)
        print("not implemented")

    def get_transaction(self, stub, transaction_hash, include_token_metadata = True):
        """Returns a transaction given a transaction hash with option to
        include token metadata
        """
        req = pb.GetTransactionRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(transaction_hash)[::-1]
        req.include_token_metadata = include_token_metadata
        return stub.GetTransaction(req)

    def get_raw_transaction(self, stub, transaction_hash):
        """ Returns a serialized transaction given a transaction hash.
        """
        req = pb.GetRawTransactionRequest()
        # hash reversed and converted to bytes
        req.hash = bytes.fromhex(transaction_hash)[::-1]
        return stub.GetRawTransaction(req)

    
    # FIXME, not complete
    def get_address_transactions(self, stub, address):
        """ Returns the transactions for the given address. Offers offset,
        limit, and from block options.
        takes 'address', 'hash', 'height', 'nb_fetch', 'nb_skip
        """
        req = pb.GetAddressTransactionsRequest()
        req.address = address
        return stub.GetAddressTransactions(req)

    def get_address_raw_transactions(self, address):
        """ Returns the serialized raw transactions for the given address.
        Offers offset, limit, and from block options.
        """
        req = pb.GetRawAddressTransactionsRequest()
        req.address = address
        return stub.GetRawAddressTransactions(req)


    # FIXME, not complete
    def get_address_unspet_outputs(self, stub, address):
        """ Returns all the unspent transaction outputs the given address.
        arguments 'address', 'include_mempool', 'include_token_metadata'
        """
        req = pb.GetAddressUnspentOutputsRequest()
        req.address = address
        return stub.GetAddressUnspentOutputs(req)

    # FIXME, not complete
    def get_unspent_output(self, stub, utxo_hash_byte):
        """ Takes an unspent output in the utxo set and returns the utxo metadata or not found.
        arguments 'hash', 'include_mempool', 'include_token_metadata', 'index'
        currently accpets byte format of the utxo hash
        """
        req = pb.GetUnspentOutputRequest()
        req.hash = utxo_hash_byte
        return stub.GetUnspentOutput(req)

    def get_merkle_proof(self, stub, transaction_hash):
        """ Returns a Merkle (SPV) proof for a specific transaction in the provided block.
        """
        req = pb.GetMerkleProofRequest()
        # hash reversed and converted to bytes
        req.transaction_hash = bytes.fromhex(transaction_hash)[::-1]
        return stub.GetMerkleProof(req)
    

    def get_slp_token_metadata(self, stub, token_ids):
        """ GetSlpTokenMetadata return slp token metadata for one or more tokens.
        """
        req = pb.GetSlpTokenMetadataRequest()
        # hash converted to bytes
        req.token_ids.append(bytes.fromhex(token_ids))
        return stub.GetSlpTokenMetadata(req)

    # FIXME - Not implemented yest
    def get_slp_parsed_script(self, stub):
        """ GetSlpParsedScript returns marshalled object from parsing
        an slp pubKeyScript using goslp package.  This endpoint does not
        require SlpIndex.
        """
        req = pb.GetSlpParsedScriptRequest()
        #print(dir(req))
        #return stub.GetSlpParsedScript(req)
        print("not implemented")


    # FIXME - Not implemented yet
    def get_slp_trusted_validation(self, stub):
        """ Returns slp validity related information for one or more transactions.
        """
        req = pb.GetSlpTrustedValidationRequest()
        #print(dir(req))
        #return stub.GetSlpTrustedValidation(req)
        print("not implemented")
    
    # FIXME - make sure outupt is correct
    def get_slp_graph_search(self, stub, transaction_hash):
        """ Returns all the transactions needed for a client to validate an SLP graph
        """
        req = pb.GetSlpGraphSearchRequest()
        req.hash = bytes.fromhex(transaction_hash)[::-1]
        return stub.GetSlpGraphSearch(req)


    def check_slp_transaction(self, stub, transaction):
        """ Checks the validity of a supposed slp transaction before it is broadcasted.
        """
        req = pb.CheckSlpTransactionRequest()
        # transfer raw transaction from hex to bytes
        transaction = bytes.fromhex(transaction)
        req.transaction = transaction
        return stub.CheckSlpTransaction(req)
        
    
    # FIXME got transaction error, grpc_message":"tx rejected: transaction
    # *** is not standard: transaction is not finalized","grpc_status":3}"
    def submit_transaction(self, stub, transaction):
        """ Submit a transaction to all connected peers.
        """
        req = pb.SubmitTransactionRequest()
        # transfer raw transaction from hex to bytes
        req.transaction = bytes.fromhex(transaction)
        return stub.SubmitTransaction(req)
        

    def subscribe_transactions(self, stub, address):
        """ SubscribeTransactions creates subscription to all relevant
        transactions based on the subscription filter.
        source: https://github.com/gcash/bchd/blob/master/bchrpc/documentation/client-usage-examples/python-grpc/address_subscription.py
        """
        ## Monitor address for incoming txs
        ## Ideally this should run in a separate thread since this is a blocking call
        txFilter = pb.TransactionFilter()
        txFilter.addresses.append(address)
        # txFilter.all_transactions = True

        req = pb.SubscribeTransactionsRequest()
        req.include_in_block = True
        req.include_mempool = True
        req.subscribe.CopyFrom(txFilter)
        print("\nMonitor txs for address: " + address + "\n")
        for notification in stub.SubscribeTransactions(req):
            tx = notification.unconfirmed_transaction.transaction

            incoming = 0
            outgoing = 0
            for txInput in tx.inputs:
                if (txInput.address == address):
                    outgoing += txInput.value

            for txOutput in tx.outputs:
                if (txOutput.address == address):
                    incoming += txOutput.value

            print("%s => INCOMING: %14d sats - OUTGOING: %14d sats" % (bytearray(tx.hash[::-1]).hex(), incoming, outgoing))


    # FIXME - Not implemented yet
    def subscribe_transactions_stream(self, stub):
        """Subscribes to relevant transactions based on the subscription
        requests. the parameters to filter transactions on can be updated
        by sending new SubscribeTransactionsRequest objects on the stream.
        """
        req = pb.SubscribeTransactionsStreamRequest()
        #return stub.SubscribeTransactionStream(req)
        print("not implemented")


    # FIXME - Not implemented yet
    def subscribe_blocks(self, stub):
        """ SubscribeBlocks creates a subscription for notifications of new blocks being
        """
        req = pb.SubscribeBlocksRequest()
        #return stub.SubscribeBlocks(req)
        print("not implemented")
            

