# BCHD GRPC python Library

A Python library to help understand and use the GRPC provided by BCHD Bitcoin Cash full node

Example for using the library:

```python
from bchd_grpc_lib import HitGrpc

 
block_height = 1
x = HitGrpc()
c = x.call_channel(x.get_block_info_by_height)(block_height)
print(c)
```

Example to get an address transactions:

```python
...
address = "qqavvzp43g0y2dez0q72er57fym4dxw37c432gdj0d"
c = x.call_channel(x.get_address_transactions)(address)
...
```

Example to get transaction data:
```python
...
transaction_hash = "79bf07128d779476184f38f0222b30e2cda887894d6c97df95da7a52343f2ca7"
c = x.call_channel(x.get_transaction)(transaction_hash)
...
```

Monitor an address for transactions
```python
...
address = "qrrpwsxwdqzr4e0l836j59n2wewmf4ze7cfdh0ntkn"
c = x.call_channel(x.subscribe_transactions)(address)
...
```
